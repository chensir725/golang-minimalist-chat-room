package main

import (
	"go_chat/logger"
	"go_chat/service"
)

func main(){
	defer logger.CloseLogFile()
	service.Run()
}
