package service

import (
	"fmt"
	"go_chat/util"
	"go_chat/worker"
	"net"
)

/**
 * 启动socket服务接收消息
 */
func Run(){
	listenSocket, err := net.Listen("tcp", "127.0.0.1:8080")
	util.CheckError(err)
	defer listenSocket.Close()
	fmt.Println("Socket Server is listening on 8080...")

	worker.Worker(listenSocket)
}
