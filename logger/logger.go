package logger

import (
	"fmt"
	"log"
	"os"
)

const (
	LOG_DIRECTORY = "G:\\Go\\src\\go_chat\\log.txt"
)
var logFile os.File
var logger *log.Logger

func init() {
	// 初始化logger
	logFile, err := os.OpenFile(LOG_DIRECTORY, os.O_RDWR | os.O_CREATE, 0)
	if err != nil {
		fmt.Println("log file create failure: " + err.Error())
		os.Exit(1)
	}
	logger = log.New(logFile, "\r\n", log.Ldate | log.Ltime | log.Llongfile)
}

func GetLogger() *log.Logger {
	return logger
}
func CloseLogFile(){
	logFile.Close()
}