package client

import (
	"bufio"
	"fmt"
	"net"
	"os"
	"strings"
)

func CheckError(err error){
	if err != nil {
		panic(err)
	}
}

func Message(conn net.Conn){
	var input  string
	for{
		reader := bufio.NewReader(os.Stdin)
		data, _, _ := reader.ReadLine()
		input = string(data)

		if strings.ToUpper(input) == "EXIT" {
			conn.Close()
			break
		}
		_, err :=conn.Write([]byte(input))
		if err != nil {
			conn.Close()
			fmt.Println("client connect failure " + err.Error())
			break
		}
	}
}

func main(){
	conn, err := net.Dial("tcp", "127.0.0.1:8080")
	CheckError(err)
	defer conn.Close()

	go Message(conn)
	buf := make([]byte, 1024)
	// 读服务端响应数据
	for {
		_, err := conn.Read(buf)
		CheckError(err)
		fmt.Printf("recieve server message content: %s\n", string(buf))
	}

	fmt.Println("client program end")
}
