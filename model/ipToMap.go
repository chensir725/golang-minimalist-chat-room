package model

import "net"

// ip地址到socket连接的映射
var OnlineConns = make(map[string] net.Conn)
