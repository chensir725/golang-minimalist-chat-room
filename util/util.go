package util

import (
	"fmt"
	"go_chat/logger"
	"os"
)

// 检查出错信息
func CheckError(err error){
	if err != nil {
		errStr := fmt.Sprintf("Error: %s\n", err.Error())
		logger.GetLogger().Println(errStr)
		fmt.Printf(errStr)
		// 程序退出
		os.Exit(1)
	}
}